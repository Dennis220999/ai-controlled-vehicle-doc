%!TEX root = ../report.tex
\section{Traffic Sign Recognition} \label{ch::traffic_sign_recognition}
The car should not only be able to detect lanes, but also the recognition and classification of traffic signs is subject of this work. Recently, several approaches of object detection using artificial neural networks have been invented. For example, \ac{frcnn} \autocite{renFasterRCNNRealTime2016} uses two distinct neural networks to find potential \acp{roi} in a first step and eventually classify objects inside these. \Ac{yolo} \autocite{redmonYouOnlyLook2016}, on the other hand, incorporates a single neural network that performs detection as well as classification at once. It gains higher execution speed, but is not capable of detecting small objects due to spatial constraints of the algorithm. Last, \ac{ssd} \autocite{liuSSDSingleShot2016a} is designed similarly to YOLO, but utilizes convolutional layers instead of fully connected layers to detect objects.

However, as the used \ac{soc} has limited computing resources, a neural network cannot run on the whole image to detect traffic signs and lights, like the aforementioned methods propose. It is necessary to detect potential classifiable candidates in a fast and tolerable error-prone way, using solely computer vision techniques. In a second step, classification can be performed on these candidates by a neural network.

There are different approaches to perform that task. Early tests have shown that a color-based approach is not robust enough to deal with different environmental conditions. Various lighting conditions in particular cause difficulties for the camera, further conditioned by non-reflecting and small traffic signs (see fig. \ref{fig::traffic_sign_candidates}).\\
However, these self-printed traffic signs have a reflective shaft\footnote{This feature has been further enhanced by adding reflective foil to the shaft and illuminating it with a lamp mounted on the vehicle.} which can be detected by the camera in an easier manner -- even under alternating lighting conditions, assuming the camera is not blinded by direct light impact. If the shaft of a traffic sign can be found, the bounding box can be placed on top of that shaft.

This section is structured in the following way:
\begin{enumerate}
    \item List all traffic signs to be detected.
    \item Discussion of performed preprocessing steps on images.
    \item Find potential shafts in the processed image.
    \item Compute suitable bounding boxes for all candidates.
\end{enumerate}

\subsection[Traffic Sign Candidates]{Traffic Sign Candidates $\dagger$} \label{ch::traffic_sign_candidates}

The following figure shows the four preselected candidates which should be classified by the neural network. The last candidate -- the traffic light -- is attached and controlled via a Raspberry Pi. Due to the three built-in \acp{led}, a distinction must be made between the resulting states for this object. This will be further explained in \cref{ch::nn_dataset}.\\

\begin{figure}[ht]
    \subcaptionbox{Stop\label{subfig::traffic_sign_candidates_stop}}%
    {\includegraphics[width=0.2\textwidth, keepaspectratio]{Pictures/traffic_signs/stop_sign.png}}
    \hspace{\fill}
    %
    \subcaptionbox{Turn left\label{subfig::traffic_sign_candidates_turn_left}}%
    {\includegraphics[width=0.2\textwidth, keepaspectratio]{Pictures/traffic_signs/turn_left.png}}
    \hspace{\fill}
    %
    \subcaptionbox{Turn right\label{subfig::traffic_sign_candidates_turn_right}}%
    {\includegraphics[width=0.2\textwidth, keepaspectratio]{Pictures/traffic_signs/turn_right.png}}
    \hspace{\fill}
    %
    \subcaptionbox{Traffic light\label{subfig::traffic_sign_candidates_light}}%
    {\includegraphics[width=0.2\textwidth, keepaspectratio]{Pictures/traffic_signs/traffic_lights.png}}
    \caption[The four traffic sign candidates]{The four traffic sign candidates.}
    \label{fig::traffic_sign_candidates}
\end{figure}

\subsection[Extraction of Bounding Boxes]{Extraction of Bounding Boxes $\dagger$}
Hough Line Transformation, used for lane detection, is too imprecise for the computation of bounding boxes, particularly in view of its computational cost. Instead, a new algorithm is designed to determine the position of vertical traffic sign shafts in images. Preliminarily, the Sobel operator is utilized to find vertical edges.

\subsubsection{The Sobel Operator}
The Sobel operator is a discrete differentiation operator, which computes an approximation of the gradient of an image's intensity function. Edges can be detected by major changes of this function. When the gradient is higher than its neighbors (or higher than a general threshold) one can see this difference as an edge in the image. The Sobel operators, included in OpenCV, combine Gaussian smoothing and differentiation, which might increase or decrease the noise resistance. The operation is performed on a gray scaled version of the image. The operator detects vertical and horizontal edges separately, thus it is very suitable for this application as the horizontal lines can be ignored. Vertical lines can be detected with the following kernel, which is multiplied with each pixel of the image \cite{opencv_sobel} \cite{howse_learning_nodate}:

\begin{equation}
    \boldsymbol{\mathrm{G_x}} = \begin{bmatrix}-1 & 0 & +1 \\ -2 & 0 & +2 \\ -1 & 0 & +1\end{bmatrix}.
\end{equation}
\clearpage
A result of this operation can be seen in figure \ref{subfig::sobel}. Since the operator also recognizes very fine lines well, these can be eliminated using a static threshold, as shown in figure \ref{subfig::sobel_threshold}.

OpenCV offers two different kinds of threshold operations: simple and adaptive thresholding.\\
With a simple threshold, every pixel is compared against one threshold value. If it is smaller than the threshold, it is set to 0, otherwise to a specified maximum value. Equation \ref{eq:threshold} shows this relation, with \textit{src(x,y)} denoting the pixel value in row \textit{y} and column \textit{x}.~\footnote{A small note about images in OpenCV: The origin of the image coordinate system is in the upper left corner, hence the row index is contrary compared to a Cartesian coordinate system.}

\begin{equation} \label{eq:threshold}
    dst(x,y) = \begin{cases}
        maxval & if \: src(x,y) > \text{thresh} \\
        0 & \text{otherwise}
    \end{cases}
\end{equation}

Adaptive thresholding is more suitable if an image is taken in different areas with different lighting conditions. An individual threshold for each pixel is calculated by the algorithm based on a small region around the pixel. In consequence, there can be multiple thresholds in one image, which may bring better results \cite{opencv_thresholding}.

The downsides are higher execution times and sometimes unpredictable results that can cause major problems in later processing steps. Because of that, the simple thresholding was chosen, producing more predictable results as long as the vehicle is driving in a reasonably homogeneous environment.

\begin{figure}
    \subcaptionbox{Grayscale image with applied Sobel operator\label{subfig::sobel}}%
    {\includegraphics[width=0.45\textwidth, keepaspectratio]{Pictures/bounding_boxes/sobel.png}}
    \hspace{\fill}
    %
    \subcaptionbox{Sobel image after thresholding\label{subfig::sobel_threshold}}%
    {\includegraphics[width=0.45\textwidth, keepaspectratio]{Pictures/bounding_boxes/thresh.png}}

    \caption[Vertical lines detected with Sobel]{Vertical edges detected with Sobel operator. One can see the fine lines of the ground, detected by this operator too. In order to reduce the number of possible lines, thresholding is applied, which results in the right picture.}
    \label{fig::sobel}
\end{figure}  

\subsubsection{Morphological Transformations}
Nevertheless, even after the thresholding, further vertical line segments remain in the image, which need to be purged (see figure \ref{subfig::sobel_threshold}). 

Morphological transformations are used to emphasize or eliminate certain shapes in images. Therefore, the image is convolved with a specific structuring element -- also called kernels. In order to close the gaps that may have been created by the thresholding, a closing cycle is performed on the binary image. Closing is defined as a Dilation followed by Erosion, which is able to remove small holes in objects \cite{opencv_morph}. The result can be seen in figure \ref{subfig::morph_closing}.

\begin{figure}[ht]
    \subcaptionbox{Closing \label{subfig::morph_closing}}%
    {\includegraphics[width=0.45\textwidth, keepaspectratio]{Pictures/bounding_boxes/morph.png}}
    \hspace{\fill}
    %
    \subcaptionbox{Erosion \label{subfig::morph_erosion}}%
    {\includegraphics[width=0.45\textwidth, keepaspectratio]{Pictures/bounding_boxes/erode.png}}

    \caption[Morphological edited pictures for shaft detection]{Morphological edited pictures for shaft detection. At first, holes of edges are removed by the Closing operation. Subsequently, noise is removed by the Erosion operation, using a special kernel (see eq. \ref{eq::erosion_kernel}).}
    \label{fig::morph_obj_detc}
\end{figure}

Remarkable are the countless small artifacts created by the so far performed operations. These can be eliminated through another Erosion with a specific kernel, shown in equation \ref{eq::erosion_kernel} below. This structuring element purges too small line segments and filters lines running too oblique, which results in figure \ref{subfig::morph_erosion}. 

\begin{equation} \label{eq::erosion_kernel}
    \boldsymbol{\mathrm{k}} = \begin{bmatrix}
        1 \\ 1 \\ 1 \\ 1 \\ 1 
    \end{bmatrix}
\end{equation}  

\subsubsection{Finding Potential Shafts}
One can clearly see the shaft of the turn left sign in figure \ref{subfig::morph_erosion}. Unfortunately, this is not the only remaining line in the processed image, whereby this is also a simple case with a smooth background and only one sign. A fast algorithm has to be developed which can deal with much more complex situations. For this purpose some sort of \ac{ransac} algorithm is implemented.

First of all, the image from figure \ref{subfig::morph_erosion} is converted into a point cloud $C$. Due to the storage sequence of an image in Python, this cloud is sorted by the row index \textit{y}. However, to perform further computations, this cloud must be sorted by the column index \textit{x}. The resulting array contains the pixel coordinates of all white points, starting with the row index. 

Now, the \ac{ransac} algorithm is applied, which picks two random points from $C$. If these points are approximately vertical (based on their \textit{x} coordinates) and if the vertical distance between them is big enough (based on their \textit{y} coordinates), the algorithm will return these points for subsequent calculations. Otherwise, two new points will be randomly chosen (the old points remain in the set).

\begin{equation} \label{eq::points_valid}
    \begin{gathered}
        \forall\;(y_1,x_1) , (y_2,x_2) \in C: (y_1,x_1) \neq (y_2,x_2):\\[6pt]
        \text{valid} = \begin{cases}
        True & \iff |x_1 -x_2| < \Delta X \land |y_1 - y_2| > \Delta Y \\
        False & \text{otherwise}
        \end{cases}
    \end{gathered}    
\end{equation}

The next step is to confirm the affiliation of both points to a line and if so, to maximize it. It is not possible to simply take all approximately equal column indices from the point cloud and count them to one line. As one can see in figure \ref{subfig::morph_erosion}, there can be smaller line segments above a shaft, which would falsify the result. 

Hence, a sliding-window algorithm is implemented, which takes the two points from \ac{ransac} and tries to find an active trace between them. Step one is to calculate the inverse slope (how much in x direction per y step) between those points to predict the next x value, according to eq. \ref{eq::inverse_slope}.

\begin{equation} \label{eq::inverse_slope}
    s = \frac{x_1-x_2}{y_1-y_2}
\end{equation}

Then, the starting point is moved downwards the binary image to be set to the origin of the shaft. If there is no active pixel at the predicted horizontal position, a sliding window examines the adjacent pixels in a fixed defined width (called $X_{WINDOW}$). As soon as an active pixel is found, the search terminates and the gap threshold is reset. This threshold ensures that small gaps, which aren't closed by the morphological operations, do not lead the algorithm to terminate too early. Besides, it is used to terminate the traversing, if there is no active pixel above the threshold. 

Resuming from the old starting point, the window is moved upwards the pixel trace according to the previously described steps. Finally, it is determined whether the end point has been found or whether the length of the line is sufficient to adopt it as a potential candidate. If one of these conditions is satisfied, the new start and end points of the line are returned. 

\begin{figure}
    \centering
    \includegraphics[height=8cm]{Pictures/bounding_boxes/start_end_point.png}
    \caption[Valid start and end point provided by \ac{ransac}]{Valid start and end point provided by \ac{ransac}\\ Blue: The starting point found by \ac{ransac}; Pink: The end point found by \ac{ransac};}
    \label{subfig::ransac_points}
\end{figure}

\begin{figure}
    \subcaptionbox{Sliding window start \label{subfig::sliding_window_start}}%
    {\includegraphics[width=0.45\textwidth, keepaspectratio]{Pictures/bounding_boxes/sliding_window_gap_one.png}}
    \hspace{\fill}
    %
    \subcaptionbox{Sliding window end \label{subfig::sliding_window_end}}%
    {\includegraphics[width=0.45\textwidth, keepaspectratio]{Pictures/bounding_boxes/sliding_window_behind_gap.png}}
    \caption[Sliding window to maximize line segment]{Sliding window to maximize line segment.\\
    Red: The calculated expected position; Cyan: A sliding window opened to find neighboring active pixels; Green: Pixel in window, which is tested active}
    \label{fig::sliding_window}
\end{figure}


To avoid finding the same lines several times, the point cloud, from which \ac{ransac} selects, is updated (see figure \ref{fig::updated_point_cloud}). All points with a column index similar to the column indices of the line's points get deleted. Hence the point cloud is sorted by the column index a binary search can be performed to find the boundary indices.

\begin{figure}
    \centering
    \includegraphics[width=.5\textwidth]{Pictures/bounding_boxes/update_point_cloud.png}
    \caption{Visualization of updated point cloud}
    \label{fig::updated_point_cloud}
\end{figure}

The whole procedure is run through several times to extract several lines of a certain length from the point cloud. Early termination only takes place if no pair of points can be found anymore which meets the condition postulated in eq. \ref{eq::points_valid}.

All empty line entries get eradicated from the list, before the next step is executed. 

\subsubsection{Positioning of Bounding Boxes}
Since a sign can be recognized in various distances, the bounding box size must be determined dynamically. The longer a detected shaft, the larger a bounding box becomes. 

Starting at the determined end point, the boxes' lower left origin will be moved slightly downwards to ensure a sufficient padding area around a sign, where the movement extent depends on the length as well. The column index of the box origin is calculated similarly. Equation \ref{eq::bounding_box_placement} shows the exact calculations. One has to specify suitable values for the bounding box offset $\epsilon$ and the bounding box size $\lambda$. 

\begin{equation} \label{eq::bounding_box_placement}
    \begin{aligned}
        l &= y_1 - y_2 \\
        BB_{y1} &= y_2 + (l \cdot \epsilon)\\
        BB_{x1} &= x_2 - (l \cdot \lambda) \\
        BB_{y2} &= BB_{y1} - (l \cdot \lambda) \\
        BB_{x2} &= BB_{x1} + (l \cdot \lambda) 
    \end{aligned}
\end{equation}

A final check to see if the points calculated in this way are still in the image ensures that the boxes are correct. Boxes which exceed the image borders will be discarded, since the assumption is made that only complete and square-shaped bounding boxes are provided to the neural network. 

The final positions of the bounding boxes are sent to the server, where they are visualized in the camera stream window, if enabled.

\subsection[Test]{Test $\dagger$}
A test shows, that the bounding boxes are quickly found in the image, even if not always correctly. As one can see in figure \ref{subfig::traffic_light_green_bad} boxes are sometimes placed below the actual traffic sign or light. Furthermore, brightly illuminated or reflective vertical edges of other objects can also result in boxes, which is not surprising at all. Thus, the neural network, discussed in section~\ref{ch::traffic_sign_classification} has to handle this kind of false report.

\begin{figure}[ht]
    \subcaptionbox{Turn Left good \label{subfig::stop_good}}%
    {\includegraphics[width=0.45\textwidth, keepaspectratio]{Pictures/bounding_boxes/recongnition/stop_good.png}}
    \hspace{\fill}
    \subcaptionbox{Turn Left bad \label{subfig::stop_bad}}
    {\includegraphics[width=0.45\textwidth, keepaspectratio]{Pictures/bounding_boxes/recongnition/stop_bad.png}}
    \caption{Bounding box example for Turn Left sign}
\end{figure}

\begin{figure}[ht]
    \subcaptionbox{Traffic Light Green good \label{subfig::traffic_light_green_good}}%
    {\includegraphics[width=0.45\textwidth, keepaspectratio]{Pictures/bounding_boxes/recongnition/light_green_good.png}}
    \hspace{\fill}
    \subcaptionbox{Traffic Light Green bad \label{subfig::traffic_light_green_bad}}
    {\includegraphics[width=0.45\textwidth, keepaspectratio]{Pictures/bounding_boxes/recongnition/light_green_bad.png}}
    \caption{Bounding box example for Traffic Light Green}
\end{figure}
