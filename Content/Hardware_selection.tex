%!TEX root = ../report.tex
\chapter{Implementation} \label{ch:implementation}

In this chapter, the implementation of the individual project parts is explained. As aforementioned, its structure follows the structure of tasks being defined in \cref{ch:tasks}.

\section{Hardware Setup} \label{ch:hardware_setup}

The first section deals with the development of a hardware platform that is to be used as vehicle later on. In detail, this is divided into two steps, where the first one concerns the selection of proper components and the second one relates to their assembly. Moreover, calibration of the used camera is described.

\subsection[Hardware Selection]{Hardware Selection $\ast$ $\dagger$} \label{ch:hardware}
\subsubsection{Mad Fighter Chassis and Components} \label{ch:mad_fighter_and_components}
The selection of the chassis is based on several criteria. These are listed below.
\begin{itemize}
    \item Price
    \item Pre-installed components
    \item Steering type
    \item Drive type
    \item Shipping duration
\end{itemize}

Since the SoC and other components are going to consume a great part of the budget, the chassis should not cost more than necessary. There are chassis on market, which are specially designed to fit \acp{soc} and other hardware components for an autonomous vehicle. They start at approx. 40€ and go up to 100€ and more. Most of them are shipped with DC-motors, \ac{esc}'s and servos and fit the Raspberry Pi.

A significant subset has a rotation-speed-based steering system. This is an undesired property. The control of the front wheel axle should be close to that of a real car. Furthermore, many chassis have a separated motor per wheel, which is also quite different in today's cars (nevertheless there are first platforms that have two or more engines). Last, adjustments to the chassis can not be ruled out, so a short shipment is desirable.

Taking into account all the mentioned points, the choice fell on the Mad Fighter chassis. It's for free, is ready to drive (all components installed), has a steering servo connected to the front axle and has no shipping time.
The orange top can be detached from the base, the latter can accommodate all components. As shown in chapter \ref{ch:design_modular_chassis}, additional attachments have to be printed to carry all parts.
\Cref{tab:mad_fighter_chassis} shows information about the chassis and installed components.

\begin{table}[ht]
    \begin{tabularx}{\textwidth}{|X||X|}
        \hline
        \textbf{Wheelbase}      & 23 mm       \\\hline
        \textbf{Length}         & 390 mm      \\\hline
        \textbf{Width}          & 236 mm      \\\hline
        \textbf{Weight}         & 990 g       \\\hline
        \textbf{Motor}          & DC, brushed \\\hline
        \textbf{Drive}          & 2WD, rear   \\\hline
        \textbf{Battery}        & NiMH 7.2V   \\\hline
        \textbf{Steering Servo} & Analog      \\\hline
    \end{tabularx}
    \caption[Mad Fighter chassis information]{Mad Fighter chassis information \cite{chassis}.}
    \label{tab:mad_fighter_chassis}
\end{table}

\subsubsection{The \acs{soc} Platform} \label{ch:computer_platform}

Several single board computers come into question to gather sensor data, perform computations and control the vehicle. \Cref{tab:soc_platforms_comparison} provides a comparison of the NVIDIA
Jetson Nano Developer Kit\footnote{In the following,  \textit{NVIDIA Jetson Nano} is used synonymously for \textit{NVIDIA Jetson Nano Developer Kit}.}, the Raspberry Pi 4B in combination  with the Coral Stick and the Coral Dev Board. 

\begin{table}[ht]
    \begin{tabularx}{\textwidth}{|X||X|X|X|}
        \hline
                                           & \textbf{NVIDIA Jetson Nano Developer Kit} \cite{jetson_specs}\cite{jetson_guide} & \textbf{Raspberry Pi 4 B + Coral Stick} \cite{pi_specs}\cite{pi_guide}\cite{coral_usb} & \textbf{Coral Dev Board} \cite{coral_board}        \\
        \hline
        \hline
        \textbf{CPU}                       & Quad-Core ARM A57 @ 1.43~GHz                                                      & Quad-Core ARM A72 @ 1.5~GHz                                                             & Quad-Core ARM A53 @ 1.5~GHz, ARM M4                \\
        \hline
        \textbf{RAM (GBytes)}                 & 4                                                                                & 2 / 4 / 8                                                                              & 4                                                  \\
        \hline
        \textbf{I/O}                       & \acs{gpio} (2$\times$\acs{pwm}), \acs{i2c}, I\textsuperscript{2}S, \acs{spi}, \acs{uart}, \acs{usb}                              & \acs{gpio} (4$\times$\acs{pwm}), \acs{i2c} I\textsuperscript{2}S, \acs{spi}, \acs{uart}, \acs{usb}                                     & \acs{gpio} (4$\times$\acs{pwm}), \acs{i2c}, I\textsuperscript{2}S, \acs{spi}, \acs{uart}T, \acs{usb} \\
        \hline
        \textbf{OS \& Software}            & Ubuntu, various frameworks for machine learning                                  & various Linux distributions, limited to TensorFlow lite                                & Mendel OS, limited to TensorFlow lite              \\
        \hline
        \textbf{Camera ports}              & 2$\times$\acs{mipi} CSI-2                                                                     & 1$\times$\acs{mipi} CSI-2                                                                           & 1$\times$\acs{mipi} CSI-2                                       \\
        \hline
        \textbf{Size in mm ($W \times L$)} & $100\times80$                                                                    & $85\times56$ (Pi), $65\times30$ (Coral)                                                & $85\times56$                                       \\
        \hline
        \textbf{Power consumption in W}    & 5 -- 25                                                                          & 15 (Pi), 2.5 -- 4.5  (Coral)                                                           & 10 -- 15                                           \\
        \hline
        \textbf{Price in €}                & approx. 85                                                                       & approx. 110                                                                            & approx. 110                                        \\
        \hline
    \end{tabularx}
    \caption[Comparison of the three  SoC platforms]{Comparison of the three selected SoC platforms.}
    \label{tab:soc_platforms_comparison}
\end{table}

It shows that all platforms have similar \acp{cpu}, but the Coral Dev Board additionally contains the ARM Cortex M4, a processor optimized for signal processing.
Besides, all of them have several options for input and output as well as at least one MIPI CSI-2 port \cite{mipi_csi} for camera connection. Thus, all the presented systems satisfy the requirements to control a model car in principle. 

The biggest difference between those SoCs relates to the support of machine learning. On the one hand, the Coral \ac{tpu} built into the Coral Stick and Coral DevBoard delivers higher performance for inference, which is achieved through several optimization techniques, but also some restrictions: One can only use TensorFlow Lite models that are limited regarding supported operations. Furthermore, weights are quantized from float32 to int8, which may result in loss of accuracy \cite{coral_tpu}. The NVIDIA Jetson Nano on the other hand offers lower inference performance, but should still be sufficiently fast. Foremost, training as well as inference is way more flexible, since one is not restricted to TensorFlow Lite, but can use other ML frameworks too (e.g. PyTorch).
In addition, an optimization \ac{sdk} called TensorRT \cite{tensorrt} is maintained, enabling acceleration of neural network inference on NVIDIA \acp{gpu}.

All in all the NVIDIA Jetson Nano proves to be the best choice due to its low
price, support of various machine learning frameworks and the option to connect
two cameras over MIPI CSI-2.

\subsubsection{Sensors} \label{ch:sensor_selection}
Besides the steering and speed control, an autonomously driving vehicle needs various sensors to determine the state of its environment, e.g. \ac{lidar} or ultrasonic sensors for distance measurement.
The former one is widely used in industrial applications as well as in autonomously driving cars and generates a very detailed image of the environment.
However, this also requires very high computing capacities and has a large space requirement \cite{overview_lidar}.

Ultrasonic sensors, in turn, do not generate that much information about their surroundings, because they only measure the distance to one point at the time. Thereby, they perform fewer operations and are better suited for embedded systems like the NVIDIA Jetson Nano.

Since the ultrasonic sensors acquire only little information about the car's environment, an additional camera is essential. Furthermore, it is specifically needed for visual detection of road markings and traffic signs.