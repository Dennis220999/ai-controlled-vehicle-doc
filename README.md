# AI controlled vehicle doc

Documentation of AI controlled vehicle project
## Rules for branching
-  one branch per section (if one person writes consectuive sections it is allowed to use one branch for all of these sections)
- branch naming convention: ```sec_<name of starting section>```
- before merge: open merge request and make all members **reviewers**.
- use the branch ``pdf`` for compiled report (if necessary)
## Rules
### Acronyms
Add acronyms in [acronyms.tex](./Input/acronyms.tex)
Add the longest acronym as argument after \begin{acronyms} to keep the sorting correct.
### Content
Add files chapter wise in the [Content](./Content)
folder.
Include them in [report.tex](./report.tex).
### Pictures
Add pictures only to the picture folder.
### Labels
The labels consist of a fixed prefix and an individual suffix. The definition of prefixes follows. The chapter prefix also applies to sections and subsections. Try to use talkative suffixes.

|Kind|Prefix|Example|
| :---| :-----|:-------|
|Chapter|ch:|ch:software|
|Picture|fig:|fig:jetson|
|Table|tab:|tab:soc_compare|
|Equations|eq:|eq:compute_stuff|
|Listings|lst:|lst:hello_world|



### Before commit 
Format your document with [latexindent.pl](https://github.com/cmhughes/latexindent.pl). Visual Studio Code's LaTeX Workshop extension will install this program automatically. Nevertheless, you need Perl installed on your system.
## Settings
Loading packages and making settings is only allowed in report.tex.