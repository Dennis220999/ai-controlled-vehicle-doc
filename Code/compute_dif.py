
"""Computes the inverted difference of nearest-neighbor and cubic interpolation results
to linear interpolation result for undistortion."""

import cv2
import numpy as np

def compute_difference(img1, img2):
    """Convert images to grayscale and ompute absolute difference between 
    two images.
    """
    img1_grey = cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY)
    img2_grey = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)
    img3_grey = cv2.absdiff(img1_grey, img2_grey)
    return img3_grey

def norm_and_invert(img, factor):
    """Normalize image values to range [0, int(255*factor)] and invert the result.
    """
    normalized = cv2.normalize(img, None, 0, int(255*factor), cv2.NORM_MINMAX)
    inverted = 255-normalized
    return inverted

if __name__ == "__main__":
    roi = np.genfromtxt("roi.csv", dtype=np.int32)
    x,y,w,h = roi
    start_range = 1
    end_range = 130
    nearest_img = cv2.imread("../Pictures/interpolation/lanes_interp_nearest.png")
    cubic_img = cv2.imread("../Pictures/interpolation/lanes_interp_cubic.png")
    linear_img = cv2.imread("../Pictures/interpolation/lanes_interp_linear_orig.png")
    nearest_img = nearest_img[y:y+h, x:x+w]
    linear_img = linear_img[y:y+h, x:x+w]
    cubic_img = cubic_img[y:y+h, x:x+w]
    nearest_diff = compute_difference(nearest_img, linear_img)
    nearest_diff = nearest_diff[start_range:end_range, start_range:end_range]
    cubic_diff = compute_difference(cubic_img, linear_img)
    cubic_diff = cubic_diff[start_range:end_range, start_range:end_range]
    
    if nearest_diff.max() > cubic_diff.max():
        factor_nearest = 1
        factor_cubic = nearest_diff.max() / 255
    else:
        factor_nearest = cubic_diff.max()
        factor_cubic = 1
    
    inv_normalized_nearest = norm_and_invert(nearest_diff, factor_nearest)
    inv_normalized_cubic = norm_and_invert(cubic_diff, factor_cubic)
    
    # create border 
    final_nearest = np.pad(inv_normalized_nearest, pad_width=2, mode='constant', constant_values=100)
    final_cubic = np.pad(inv_normalized_cubic, pad_width=2, mode='constant', constant_values=100)
    
    # plot diffence
    cv2.imshow("difference of nearest and linear", final_nearest)
    cv2.imshow("difference of cubic and linear", final_cubic)
    cv2.waitKey()
    
    # plot a red rectangle into the linear interpolated image and save it
    cv2.rectangle(linear_img, (y+start_range, x+start_range), (y+end_range, x+end_range), (0, 0, 255), 2)
    
    # save difference images
    cv2.imwrite("../Pictures/interpolation/lanes_dif_linear_nearest.png", final_nearest)
    cv2.imwrite("../Pictures/interpolation/lanes_dif_linear_cubic.png", final_cubic)
    cv2.imwrite("../Pictures/interpolation/lanes_interp_linear_marked.png", linear_img)